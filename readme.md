#Documentation:
- create a dist folder
-create an auth.json and add a token field where you paste your discord auth token
- npm run build-ts
- npm run watch
- ???
- profits!

#Code Guidelines:

- this project use the web module Philosophy
- every module is a folder that contains a starting middleware with sub related classes , definitions and data classes
- every module is self contained and require no depedencies others than the ones included in its folder or general npm 
dependancies

- Modules should be designed using DRY principles, make sure they are reusable as much as possible
- Modules take an config object that give it its initial parameters.
- Modules exposes a public api of middleware functions , ideally functions should be as pure as possible 
( sahould ideally use parameters and return values, unless it absolutely needs to use the modules internal state)
