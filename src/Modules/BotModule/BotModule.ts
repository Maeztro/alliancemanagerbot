const discord = require('discord.js');

export class BotModule {
    protected discordClient;
    constructor(config?: any){

        const Discord = require('discord.js');
        this.discordClient = new Discord.Client();

        this.discordClient.on('ready', () => {
            console.log(`Logged in as ${this.discordClient.user.tag}!`)
        });

        this.discordClient.on('message', msg => {
            if (msg.content === 'ping') {
                msg.reply('Pong!')
            }
        });

        this.discordClient.login(config.token);
        
    }
}
