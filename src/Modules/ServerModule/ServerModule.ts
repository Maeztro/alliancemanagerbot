import {Request, Response} from 'express';
import express from 'express';
import bodyParser from 'body-parser';

export class ServerModule {

    protected app: any;
    protected server: any;
    protected port: number;

    constructor( config?: any){
        this.port = config.port;

        this.createExpressApp();
        this.setPort(this.port);
        this.createServer(this.app);

        if ( config.cors === true ){
            this.enableAppCors();
        }

        this.setHttpRoutes(config.route);

    }

    public createExpressApp() {
        this.app = express();
        this.app.use(bodyParser.json());
    }

    public enableAppCors() {
        this.app.use((req: Request, res: Response, next: any) => {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE, OPTIONS");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Auth-Token");
            next();
        });
    }

    public setPort(port?: number){
        this.app.set("port", process.env.PORT || port || 3000);
    }

    public createServer(app: any) {
        this.server = require('http').createServer(app);
    }

    //loop through config and setup middlewares to handle each requests
    public setHttpRoutes(routeList: any){
        routeList.forEach( (routeInstance: any) => {
            this.app.use('/'+ routeInstance.name, routeInstance.route);
        });
    }

    public startServer(){
        this.server.listen(8080, () => console.log('Alliance Bot listening on port 8080!'));
    }
}
