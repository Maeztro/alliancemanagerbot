import {ServerModule} from "./Modules/ServerModule/ServerModule";
import {QueryModule} from "./Modules/QueryModule/QueryModule";
import {BotModule} from "./Modules/BotModule/BotModule";

const auth = require('../auth.json');

const defaultRoute = new QueryModule()

const routes = [
    {name:"api", route: defaultRoute },
];

const discordBot = new BotModule({token:auth.token});

const serverModule = new ServerModule({
    port: 3000,
    cors: true,
    route: routes
});

serverModule.startServer();
